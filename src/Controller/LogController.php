<?php

namespace Drupal\my_services_demo\Controller;

/**
 * @file
 * Contains \Drupal\my_services_demo\Controller\LogController.
 */

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class LogController.
 */
class LogController extends ControllerBase {

  /**
   * Logs one message of each type to the logger.
   *
   * @return array
   *   Render array containing our message.
   */
  public function loggingDemo() {

    $logger = $this->getLogger('my_services_demo');

    // Emergency level log. Example: System is unusable.
    $logger->emergency('Emergency message');

    // Alert level log. Example: Action must be taken immediately.
    $logger->alert('Alert message');

    // Critical conditions. Example: Application component unavailable,
    // unexpected exception.
    $logger->critical('Critical message');

    // Error. Example: Runtime errors that do not require immediate action but
    // should typically be logged and monitored.
    $logger->error('Error message');

    // Exceptional occurrences that are not errors.
    // Example: Use of deprecated APIs, poor use of an API, undesirable things
    // that are not necessarily wrong.
    $logger->warning('Warning message');

    // Normal but significant events.
    $logger->notice('Notice message');

    // Interesting events. Example: User logs in, SQL logs.
    $logger->info('Info message');

    // Detailed debug information.
    $logger->debug('Debug message');

    $link = Link::fromTextAndUrl('recent log messages', Url::fromRoute('dblog.overview'));

    return [
      '#markup' => $this->t('If you have permission to see it, you can navigate to @link to see the new log messages', ['@link' => $link->toString()]),
    ];
  }

}
