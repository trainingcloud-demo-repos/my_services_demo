<?php

namespace Drupal\my_services_demo\Controller;

/**
 * @file
 * Contains \Drupal\my_services_demo\Controller\TranslationController.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Class TranslationController.
 */
class TranslationController extends ControllerBase {

  /**
   * Demonstrates Drupal's Translation service.
   *
   * @return array
   *   Render array containing our message.
   */
  public function translationDemo() {

    $msg1 = $this->t('Params with the @ placeholder are @param.', [
      '@param' => '<i>sanitised</i> for display to the user',
    ]);

    $msg2 = $this->t('Params with the % placeholder are %param.', [
      '%param' => 'wrapped in <em> tags',
    ]);

    $msg3 = $this->t('Params with the : placeholder are intended for 
    URLs, and strip dangerous protocols, eg: :param.', [
      ':param' => 'javascript://example.com',
    ]);

    return [
      '#markup' => "<p>$msg1</p><p>$msg2</p><p>$msg3</p>",
    ];
  }

  /**
   * Demonstrates multiple placeholders.
   *
   * @return array
   *   Render array containing a message with more than one placeholder.
   */
  public function multiplePlaceholdersDemo() {
    // Hardcode the read and unread messages count.
    // In a real scenario we would create and use custom functions
    // that retrieve these values from somewhere.
    $read = 42;
    $unread = 230;

    $msg = $this->t('You have @unread unread messages and @read read messages', [
      '@read' => $read,
      '@unread' => $unread,
    ]);

    return [
      '#markup' => $msg,
    ];
  }

}
