<?php

namespace Drupal\my_services_demo\Controller;

/**
 * @file
 * Contains \Drupal\my_services_demo\Controller\ConfigController.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ConfigController.
 */
class ConfigController extends ControllerBase {

  /**
   * Reads values from config and displays them to the user.
   *
   * @return array
   *   Render array containing our message.
   */
  public function configDemo() {

    $config = $this->config('system.site');

    return [
      '#markup' => $this->t('Welcome to @sitename, you can reach us at @email.', [
        '@sitename' => $config->get('name'),
        '@email' => $config->get('mail'),
      ]),
    ];
  }

}
