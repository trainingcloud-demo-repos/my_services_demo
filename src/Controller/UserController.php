<?php

namespace Drupal\my_services_demo\Controller;

/**
 * @file
 * Contains \Drupal\my_services_demo\Controller\UserController.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Class UserController.
 */
class UserController extends ControllerBase {

  /**
   * Says hello to the current user.
   *
   * @return array
   *   Render array containing our message.
   */
  public function currentUserDemo() {

    $currentUser = $this->currentUser();

    return [
      '#markup' => $this->t('Hello @name', ['@name' => $currentUser->getDisplayName()]),
    ];
  }

}
