<?php

namespace Drupal\my_services_demo\Controller;

/**
 * @file
 * Contains \Drupal\my_services_demo\Controller\AlertController.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Class AlertController.
 */
class AlertController extends ControllerBase {

  /**
   * Displays the different types of available alerts.
   *
   * @return array
   *   Render array containing our message.
   */
  public function alertsDemo() {

    $messenger = $this->messenger();

    $messenger->addStatus($this->t("This is a status message"));
    $messenger->addWarning($this->t("This is a warning message"));
    $messenger->addError($this->t("This is a error message"));

    return [
      '#markup' => $this->t('This is the page content.'),
    ];
  }

}
